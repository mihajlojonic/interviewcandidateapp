use interviewappdb;  
---------------------
--POSITION
---------------------
if OBJECT_ID('dbo.Position','U') is not null
	drop table Position;

create table Position 
(
	positionId int identity(1,1) not null primary key,
	positionName varchar(100) not null,
	positionDescription varchar(512) not null,
	positionMinimumSeniority varchar(10) not null 
							 check (positionMinimumSeniority 
							 in('junior','medior','senior'))
);

insert Position(positionName, positionDescription, positionMinimumSeniority)
values('SQL Developer','Description for position SQL Developer','junior');

insert Position(positionName, positionDescription, positionMinimumSeniority)
values('C# Developer','Description for position C# Developer','medior');

insert Position(positionName, positionDescription, positionMinimumSeniority)
values('C++ Developer','Description for position C++ Developer','senior'); 

---------------------
--CANDIDATE
---------------------                           
if OBJECT_ID('dbo.Candidate','U') is not null
	drop table Candidate;
create table Candidate
(
	candidateId int identity(1,1) not null primary key,
	firstName varchar(50) not null,
	lastName varchar(50) not null,
	email varchar(100) not null,
	mobilePhone varchar (30) not null,
	candidateSeniority varchar(10) not null
					   check (candidateSeniority
					   in('junior','medior','senior'))
);            

insert Candidate(firstName, lastName, email, mobilePhone, candidateSeniority)
values('Mladen','Mladenovic','mm@mail.com','060-000-000','junior');          

insert Candidate(firstName, lastName, email, mobilePhone, candidateSeniority)
values('Nikola','Nikolic','nn@mail.com','060-000-001','medior');

insert Candidate(firstName, lastName, email, mobilePhone, candidateSeniority)
values('Jovan','Jovanovic','jj@mail.com','060-000-002','senior');

---------------------
--INTERVIEWER
---------------------
if OBJECT_ID('dbo.Interviewer', 'U') is not null
	drop table Interviewer;
create table Interviewer
(
	interviewerId int identity(1,1) not null primary key,
	username varchar(50) not null,
	password varchar(50) not null,
	firstName varchar(50) not null,
	lastName varchar(50) not null,
	email varchar(100) not null,
	mobilePhone varchar(30) not null
);

insert Interviewer(username, password, firstName, lastName, email, mobilePhone)
values('mm', 'pass','Milos','Milosevic','milmil@mail.com','060-000-003');

insert Interviewer(username, password, firstName, lastName, email, mobilePhone)
values('mih', 'pass','Mihajlo','Mihajlovic','mihmih@mail.com','060-000-004');

insert Interviewer(username, password, firstName, lastName, email, mobilePhone)
values('nen', 'pass', 'Nenad','Nenadovic','nennen@mail.com','060-000-005');

---------------------
--INTERVIEW
---------------------
if OBJECT_ID('dbo.Interview', 'U') is not null
	drop table Interview;
create table Interview
(
	interviewId int identity(1,1) not null primary key,
	positionId int not null,
	candidateId int not null,
	interviewerId int not null
);

alter table Interview
add constraint FK_Interview_Position foreign key (positionId)
references dbo.Position(positionId);

alter table Interview
add constraint FK_Interview_Candidate foreign key (candidateId)
references dbo.Candidate(candidateId);

alter table Interview
add constraint FK_Interview_Interviewer foreign key (interviewerId)
references dbo.Interviewer(interviewerId);


---------------------
--ROUND
---------------------
if OBJECT_ID('dbo.Round', 'U') is not null
	drop table InterviewRound;
create table InterviewRound
(
	interviewRoundId int identity(1,1) not null primary key,
	interviewRoundTitle varchar(100) not null,
	interviewRoundDescription varchar(512) not null,
	interviewRoundOrder int not null,
	interviewRoundDate date not null,
	interviewRoundDuration int not null,
	interviewId int not null
);                                     

alter table InterviewRound
add constraint FK_InterviewRound_Interview foreign key(interviewId)
references dbo.Interview(interviewId);

--insert InterviewRound(interviewRoundTitle, interviewRoundDescription, interviewRoundOrder)
--values('Phone call round','Description for interview round phone call',1);

--insert InterviewRound(interviewRoundTitle, interviewRoundDescription, interviewRoundOrder)
--values('HR round','Description for interview round HR round',2);

--insert InterviewRound(interviewRoundTitle, interviewRoundDescription, interviewRoundOrder)
--values('Coding round','Description for interview round coding round',2);

--insert InterviewRound(interviewRoundTitle, interviewRoundDescription, interviewRoundOrder)
--values('Final conversation round','Description for interview round final conversation',2);

---------------------
--COMMENT
---------------------
if OBJECT_ID('dbo.Comment','U') is not null
	drop table Comment;
create table Comment
(
	commentId int identity(1,1) not null primary key,
	commentText varchar(512) not null,
	Rate int check(Rate<10 and Rate>0),
	roundId int not null
);

alter table Comment
add constraint FK_Comment_InterviewRound foreign key(roundId)
references dbo.InterviewRound(interviewRoundId);