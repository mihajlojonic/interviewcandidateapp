﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCandidateAppApi.Controllers
{
    public class InterviewRoundController : Controller
    {
        IRepositoryWrapper _repository;

        public InterviewRoundController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllInterviewRounds()
        {
            try
            {
                var interviewRounds = await _repository.InterviewRound.GetAllInterviewRoundsAsync();
                return Ok(interviewRounds);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpGet("{id}", Name = "InterviewRoundById")]
        public async Task<IActionResult> GetInterviewRoundById(int id)
        {
            try
            {
                var interviewRound = _repository.InterviewRound.GetInterviewRoundByIdAsync(id);

                if (interviewRound == null)
                    return NotFound();
                else
                    return Ok(interviewRound);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateInterviewRound([FromBody]InterviewRound interviewRound)
        {
            try
            {
                if (interviewRound == null)
                    return BadRequest("InterviewerRound object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                await _repository.InterviewRound.CreateInterviewRoundAsync(interviewRound);
                return CreatedAtRoute("InterviewById", new { id = interviewRound.InterviewRoundId }, interviewRound);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateInterviewRound(int id, [FromBody]InterviewRound interviewRound)
        {
            try
            {
                if (interviewRound == null)
                    return BadRequest("InterviewRound object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                var dbInterviewRound = await _repository.InterviewRound.GetInterviewRoundByIdAsync(id);

                if (dbInterviewRound == null)
                    return NotFound();

                await _repository.InterviewRound.UpdateInterviewRoundAsync(dbInterviewRound, interviewRound);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInterviewRound(int id)
        {
            try
            {
                var interviewRound = await _repository.InterviewRound.GetInterviewRoundByIdAsync(id);
                if (interviewRound == null)
                    return NotFound();

                await _repository.InterviewRound.DeleteInterviewRoundAsync(interviewRound);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

    }
}