﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCandidateAppApi.Controllers
{
    [Route("api/[controller]")]
    public class InterviewerController : Controller
    {
        IRepositoryWrapper _repository;

        public InterviewerController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllInterviewers()
        {
            try
            {
                var interviewers = await _repository.Interviewer.GetAllInterviewersAsync();
                return Ok(interviewers);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpGet("{id}", Name = "InterviewerById")]
        public async Task<IActionResult> GetInterviewerById(int id)
        {
            try
            {
                var interviewer = await _repository.Interviewer.GetInterviewerByIdAsync(id);

                if (interviewer == null)
                    return NotFound();
                else
                    return Ok(interviewer);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateInterviewer([FromBody]Interviewer interviewer)
        {
            try
            {
                if (interviewer == null)
                    return BadRequest("Interviewer object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                await _repository.Interviewer.CreateInterviewerAsync(interviewer);
                return CreatedAtRoute("InterviewerById", new { id = interviewer.InterviewerId }, interviewer);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateInterviewer(int id, [FromBody]Interviewer interviewer)
        {
            try
            {
                if (interviewer == null)
                    return BadRequest("Interviewer object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                var dbInterviewer = await _repository.Interviewer.GetInterviewerByIdAsync(id);
                if (dbInterviewer == null)
                    return NotFound();

                await _repository.Interviewer.UpdateInterviewerAsync(dbInterviewer, interviewer);

                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInterviewer(int id)
        {
            try
            {
                var interviewer = await _repository.Interviewer.GetInterviewerByIdAsync(id);
                if (interviewer == null)
                    return NotFound();

                await _repository.Interviewer.DeleteInterviewerAsync(interviewer);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }
    }
}