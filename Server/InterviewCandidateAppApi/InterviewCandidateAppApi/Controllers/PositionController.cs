﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCandidateAppApi.Controllers
{
    public class PositionController : Controller
    {
        IRepositoryWrapper _repository;

        public PositionController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPositons()
        {
            try
            {
                var positions = await _repository.Position.GetAllPosiotionsAsync();
                return Ok(positions);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpGet("{id}", Name = "PositionById")]
        public async Task<IActionResult> GetPositionById(int id)
        {
            try
            {
                var position = await _repository.Position.GetPositionByIdAsync(id);

                if (position == null)
                    return NotFound();
                else
                    return Ok(position);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreatePosition([FromBody]Position position)
        {
            try
            {
                if (position == null)
                    return BadRequest("Position object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                await _repository.Position.CreatePositionAsync(position);
                return CreatedAtRoute("PositionById", new { id = position.PositionId }, position);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePosition(int id, [FromBody] Position position)
        {
            try
            {
                if (position == null)
                    return BadRequest("Position object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Ivalid model object");

                var dbPosition = await _repository.Position.GetPositionByIdAsync(id);
                if (dbPosition == null)
                    return NotFound();

                await _repository.Position.UpdatePositionAsync(dbPosition, position);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePosition(int id)
        {
            try
            {
                var position = await _repository.Position.GetPositionByIdAsync(id);
                if (position == null)
                    return NotFound();

                await _repository.Position.DeletePositionAsync(position);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }
    }
}