﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCandidateAppApi.Controllers
{
    [Route("api/[controller]")]
    public class CandidateController : Controller
    {
        IRepositoryWrapper _repository;

        public CandidateController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCandidates()
        {
            try
            {
                var interviewers = await _repository.Candidate.GetAllCandidates();
                return Ok(interviewers);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpGet("{id}", Name = "CanidateById")]
        public async Task<IActionResult> GetCandidateById(int id)
        {
            try
            {
                var candidate = await _repository.Candidate.GetCandidateByIdAsync(id);

                if (candidate == null)
                    return NotFound();
                else
                    return Ok(candidate);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCandidate([FromBody]Candidate candidate)
        {
            try
            {
                if (candidate == null)
                    return BadRequest("Candiate object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalida model state");

                await _repository.Candidate.CreateCandidateAsync(candidate);
                return CreatedAtRoute("CandidateById", new { id = candidate.CandidateId }, candidate);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateCandidate(int id, [FromBody]Candidate candidate)
        {
            try
            {
                if (candidate == null)
                    return BadRequest("Candidate object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalida model object");

                var dbCandidate = await _repository.Candidate.GetCandidateByIdAsync(id);
                if (dbCandidate == null)
                    return NotFound();

                await _repository.Candidate.UpdateCandidateAsync(dbCandidate, candidate);

                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCandidate(int id)
        {
            try
            {
                var candidate = await _repository.Candidate.GetCandidateByIdAsync(id);
                if (candidate == null)
                    return NotFound();

                await _repository.Candidate.DeleteCandidateAsync(candidate);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }
    }
}