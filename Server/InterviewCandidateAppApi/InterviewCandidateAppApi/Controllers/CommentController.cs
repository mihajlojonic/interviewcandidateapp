﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCandidateAppApi.Controllers
{
    public class CommentController : Controller
    {
        IRepositoryWrapper _repository;

        public CommentController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllComments()
        {
            try
            {
                var comments = await _repository.Comment.GetAllCommnetsAsync();
                return Ok(comments);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpGet("{id}", Name = "CommentById")]
        public async Task<IActionResult> GetCommentById(int id)
        {
            try
            {
                var comment = await _repository.Comment.GetCommentByIdAsync(id);

                if (comment == null)
                    return NotFound();
                else
                    return Ok(comment);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateComment([FromBody]Comment comment)
        {
            try
            {
                if (comment == null)
                    return BadRequest("Comment object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                await _repository.Comment.CreateCommentAsync(comment);
                return CreatedAtRoute("CommentById", new { id = comment.CommentId }, comment);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateComment(int id, [FromBody]Comment comment)
        {
            try
            {
                if (comment == null)
                    return BadRequest("Comment object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                var dbComment = await _repository.Comment.GetCommentByIdAsync(id);
                if (dbComment == null)
                    return NotFound();

                await _repository.Comment.UpdateCommentAsync(dbComment, comment);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            try
            {
                var comment = await _repository.Comment.GetCommentByIdAsync(id);
                if (comment == null)
                    return NotFound();

                await _repository.Comment.DeleteCommentAsync(comment);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }
    }
}