﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InterviewCandidateAppApi.Controllers
{
    [Route("api/[controller]")]
    public class InterviewController : Controller
    {
        IRepositoryWrapper _repository;

        public InterviewController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet, Authorize]
        public IEnumerable<string> Get()
        {
            return new string[] { "John Doe", "John Doe" };
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetAllInterviews()
        {
            try
            {
                var interviews = await _repository.Interview.GetAllInterviewsAsync();
                return Ok(interviews);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpGet("{id}", Name = "InterviewById"), Authorize]
        public async Task<IActionResult> GetInterviewById(int id)
        {
            try
            {
                var interview = await _repository.Interview.GetInteviewByIdAsync(id);

                if (interview == null)
                    return NotFound();
                else
                    return Ok(interview);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "internal server error " + ex);
            }
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> CreateInterview([FromBody]Interview interview)
        {
            try
            {
                if (interview == null)
                    return BadRequest("Interview object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model state");

                await _repository.Interview.CreateInterviewAsync(interview);
                return CreatedAtRoute("interviewById", new { id = interview.InterviewId }, interview);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpPut("{id}"), Authorize]
        public async Task<IActionResult> UpdateInterview(int id, [FromBody]Interview interview)
        {
            try
            {
                if (interview == null)
                    return BadRequest("Interview object is null");

                if (!ModelState.IsValid)
                    return BadRequest("Invalid model object");

                var dbInterview = await _repository.Interview.GetInteviewByIdAsync(id);
                if (dbInterview == null)
                    return NotFound();

                await _repository.Interview.UpdateInterviewAsync(dbInterview, interview);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }

        [HttpDelete("{id}"), Authorize]
        public async Task<IActionResult> DeleteInterview(int id)
        {
            try
            {
                var interview = await _repository.Interview.GetInteviewByIdAsync(id);
                if (interview == null)
                    return NotFound();

                await _repository.Interview.DeleteInterviewAsync(interview);
                return NoContent();
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex);
            }
        }
    }
}