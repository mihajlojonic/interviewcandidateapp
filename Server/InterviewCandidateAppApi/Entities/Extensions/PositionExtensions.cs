﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class PositionExtensions
    {
        public static void Map(this Position dbPosition, Position position)
        {
            dbPosition.PositionDescription = position.PositionDescription;
            dbPosition.PositionMinimumSeniority = position.PositionMinimumSeniority;
            dbPosition.PositionName = position.PositionName;
        }
    }
}
