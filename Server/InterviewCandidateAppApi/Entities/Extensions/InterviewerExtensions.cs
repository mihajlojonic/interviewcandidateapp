﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class InterviewerExtensions
    {
        public static void Map(this Interviewer dbInterviewer, Interviewer interviewer)
        {
            dbInterviewer.FirstName = interviewer.FirstName;
            dbInterviewer.LastName = interviewer.LastName;
            dbInterviewer.Username = interviewer.Username;
            dbInterviewer.Password = interviewer.Password;
            dbInterviewer.Email = interviewer.Email;
            dbInterviewer.MobilePhone = interviewer.MobilePhone;
        }
    }
}
