﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class CommentExtensions
    {
        public static void Map(this Comment dbComment, Comment comment)
        {
            dbComment.CommentText = comment.CommentText;
            dbComment.Rate = comment.Rate;
        }
    }
}
