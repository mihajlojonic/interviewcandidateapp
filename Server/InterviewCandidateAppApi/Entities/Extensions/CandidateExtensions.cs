﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class CandidateExtensions
    {
        public static void Map(this Candidate dbCandidate, Candidate candidate)
        {
            dbCandidate.FirstName = candidate.FirstName;
            dbCandidate.LastName = candidate.LastName;
            dbCandidate.CandidateSeniority = candidate.CandidateSeniority;
            dbCandidate.Email = candidate.Email;
            dbCandidate.MobilePhone = candidate.MobilePhone;
        }
    }
}
