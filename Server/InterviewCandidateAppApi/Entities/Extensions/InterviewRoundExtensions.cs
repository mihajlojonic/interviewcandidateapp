﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class InterviewRoundExtensions
    {
        public static void Map(this InterviewRound dbInterviewRound, InterviewRound interviewRound)
        {
            dbInterviewRound.InterviewId = interviewRound.InterviewId;
            dbInterviewRound.InterviewRoundDate = interviewRound.InterviewRoundDate;
            dbInterviewRound.InterviewRoundDescription = interviewRound.InterviewRoundDescription;
            dbInterviewRound.InterviewRoundDuration = interviewRound.InterviewRoundDuration;
            dbInterviewRound.Comment = interviewRound.Comment;
            dbInterviewRound.InterviewRoundOrder = interviewRound.InterviewRoundOrder;
            dbInterviewRound.InterviewRoundTitle = interviewRound.InterviewRoundTitle;
        }
    }
}
