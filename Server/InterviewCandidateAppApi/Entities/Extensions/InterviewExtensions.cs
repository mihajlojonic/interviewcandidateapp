﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class InterviewExtensions
    {
        public static void Map(this Interview dbInterview, Interview interview)
        {
            dbInterview.CandidateId = interview.CandidateId;
            dbInterview.InterviewerId = interview.InterviewerId;
            dbInterview.PositionId = interview.PositionId;
        }
    }
}
