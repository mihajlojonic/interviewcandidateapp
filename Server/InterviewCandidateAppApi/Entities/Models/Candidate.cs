﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Candidate
    {
        public Candidate()
        {
            Interview = new HashSet<Interview>();
        }

        public int CandidateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string CandidateSeniority { get; set; }

        public ICollection<Interview> Interview { get; set; }
    }
}
