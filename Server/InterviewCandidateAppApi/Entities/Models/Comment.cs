﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Comment
    {
        public int CommentId { get; set; }
        public string CommentText { get; set; }
        public int? Rate { get; set; }
        public int RoundId { get; set; }

        public InterviewRound Round { get; set; }
    }
}
