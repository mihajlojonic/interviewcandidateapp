﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Interview
    {
        public Interview()
        {
            InterviewRound = new HashSet<InterviewRound>();
        }

        public int InterviewId { get; set; }
        public int PositionId { get; set; }
        public int CandidateId { get; set; }
        public int InterviewerId { get; set; }

        public Candidate Candidate { get; set; }
        public Interviewer Interviewer { get; set; }
        public Position Position { get; set; }
        public ICollection<InterviewRound> InterviewRound { get; set; }
    }
}
