﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Position
    {
        public Position()
        {
            Interview = new HashSet<Interview>();
        }

        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public string PositionDescription { get; set; }
        public string PositionMinimumSeniority { get; set; }

        public ICollection<Interview> Interview { get; set; }
    }
}
