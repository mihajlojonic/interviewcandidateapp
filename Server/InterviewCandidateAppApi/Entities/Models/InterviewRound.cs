﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class InterviewRound
    {
        public InterviewRound()
        {
            Comment = new HashSet<Comment>();
        }

        public int InterviewRoundId { get; set; }
        public string InterviewRoundTitle { get; set; }
        public string InterviewRoundDescription { get; set; }
        public int InterviewRoundOrder { get; set; }
        public DateTime InterviewRoundDate { get; set; }
        public int InterviewRoundDuration { get; set; }
        public int InterviewId { get; set; }

        public Interview Interview { get; set; }
        public ICollection<Comment> Comment { get; set; }
    }
}
