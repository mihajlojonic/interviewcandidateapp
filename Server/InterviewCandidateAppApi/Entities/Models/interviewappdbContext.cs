﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Entities.Models
{
    public partial class interviewappdbContext : DbContext
    {
        public interviewappdbContext()
        {
        }

        public interviewappdbContext(DbContextOptions<interviewappdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Candidate> Candidate { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Interview> Interview { get; set; }
        public virtual DbSet<Interviewer> Interviewer { get; set; }
        public virtual DbSet<InterviewRound> InterviewRound { get; set; }
        public virtual DbSet<Position> Position { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(localdb)\\interviewappdb;Database=interviewappdb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Candidate>(entity =>
            {
                entity.Property(e => e.CandidateId).HasColumnName("candidateId");

                entity.Property(e => e.CandidateSeniority)
                    .IsRequired()
                    .HasColumnName("candidateSeniority")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .IsRequired()
                    .HasColumnName("mobilePhone")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.CommentId).HasColumnName("commentId");

                entity.Property(e => e.CommentText)
                    .IsRequired()
                    .HasColumnName("commentText")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.RoundId).HasColumnName("roundId");

                entity.HasOne(d => d.Round)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.RoundId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comment_InterviewRound");
            });

            modelBuilder.Entity<Interview>(entity =>
            {
                entity.Property(e => e.InterviewId).HasColumnName("interviewId");

                entity.Property(e => e.CandidateId).HasColumnName("candidateId");

                entity.Property(e => e.InterviewerId).HasColumnName("interviewerId");

                entity.Property(e => e.PositionId).HasColumnName("positionId");

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.Interview)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Interview_Candidate");

                entity.HasOne(d => d.Interviewer)
                    .WithMany(p => p.Interview)
                    .HasForeignKey(d => d.InterviewerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Interview_Interviewer");

                entity.HasOne(d => d.Position)
                    .WithMany(p => p.Interview)
                    .HasForeignKey(d => d.PositionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Interview_Position");
            });

            modelBuilder.Entity<Interviewer>(entity =>
            {
                entity.Property(e => e.InterviewerId).HasColumnName("interviewerId");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .IsRequired()
                    .HasColumnName("mobilePhone")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InterviewRound>(entity =>
            {
                entity.Property(e => e.InterviewRoundId).HasColumnName("interviewRoundId");

                entity.Property(e => e.InterviewId).HasColumnName("interviewId");

                entity.Property(e => e.InterviewRoundDate)
                    .HasColumnName("interviewRoundDate")
                    .HasColumnType("date");

                entity.Property(e => e.InterviewRoundDescription)
                    .IsRequired()
                    .HasColumnName("interviewRoundDescription")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.InterviewRoundDuration).HasColumnName("interviewRoundDuration");

                entity.Property(e => e.InterviewRoundOrder).HasColumnName("interviewRoundOrder");

                entity.Property(e => e.InterviewRoundTitle)
                    .IsRequired()
                    .HasColumnName("interviewRoundTitle")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Interview)
                    .WithMany(p => p.InterviewRound)
                    .HasForeignKey(d => d.InterviewId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InterviewRound_Interview");
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.Property(e => e.PositionId).HasColumnName("positionId");

                entity.Property(e => e.PositionDescription)
                    .IsRequired()
                    .HasColumnName("positionDescription")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.PositionMinimumSeniority)
                    .IsRequired()
                    .HasColumnName("positionMinimumSeniority")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PositionName)
                    .IsRequired()
                    .HasColumnName("positionName")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
