﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CandidateRepository : RepositoryBase<Candidate>, ICandidateRepository
    {
        public CandidateRepository(interviewappdbContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Candidate>> GetAllCandidates()
        {
            var candidates = await FindAllAsync();
            return candidates.OrderBy(x => x.FirstName);
        }

        public async Task<Candidate> GetCandidateByIdAsync(int candidateId)
        {
            var candidate = await FindByConditionAsync(o => o.CandidateId.Equals(candidateId));
            return candidate.DefaultIfEmpty(new Candidate()).FirstOrDefault();
        }

        public async Task CreateCandidateAsync(Candidate candidate)
        {
            Create(candidate);
            await SaveAsync();
        }

        public async Task UpdateCandidateAsync(Candidate dbCandidate, Candidate candidate)
        {
            dbCandidate.Map(candidate);
            Update(dbCandidate);
            await SaveAsync();
        }

        public async Task DeleteCandidateAsync(Candidate candidate)
        {
            Delete(candidate);
            await SaveAsync();
        }
    }
}
