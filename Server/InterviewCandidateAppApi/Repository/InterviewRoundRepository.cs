﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class InterviewRoundRepository : RepositoryBase<InterviewRound>, IInterviewRoundRepository
    {
        public InterviewRoundRepository(interviewappdbContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<InterviewRound>> GetAllInterviewRoundsAsync()
        {
            var interviewRounds = await FindAllAsync();
            return interviewRounds;
        }

        public async Task<InterviewRound> GetInterviewRoundByIdAsync(int interviewRoundId)
        {
            
            var interviewRound = await FindByConditionAsync(x => x.InterviewRoundId.Equals(interviewRoundId));
            return interviewRound.DefaultIfEmpty(new InterviewRound()).FirstOrDefault();
        }

        public async Task CreateInterviewRoundAsync(InterviewRound interviewRound)
        {
            Create(interviewRound);
            await SaveAsync();
        }

        public async Task DeleteInterviewRoundAsync(InterviewRound interviewRound)
        {
            Delete(interviewRound);
            await SaveAsync();
        }

        public async Task UpdateInterviewRoundAsync(InterviewRound dbInterviewRound, InterviewRound interviewRound)
        {
            dbInterviewRound.Map(interviewRound);
            Update(interviewRound);
            await SaveAsync();
        }
    }
}
