﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PositionRepository : RepositoryBase<Position>, IPositionRepository
    {
        public PositionRepository(interviewappdbContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Position>> GetAllPosiotionsAsync()
        {
            var positions = await FindAllAsync();
            return positions.OrderBy(x => x.PositionName);
        }

        public async Task<Position> GetPositionByIdAsync(int positionId)
        {
            var position = await FindByConditionAsync(x => x.PositionId.Equals(positionId));
            return position.DefaultIfEmpty(new Position()).FirstOrDefault();
        }

        public async Task CreatePositionAsync(Position position)
        {
            Create(position);
            await SaveAsync();
        }

        public async Task UpdatePositionAsync(Position dbPosition, Position position)
        {
            dbPosition.Map(position);
            Update(dbPosition);
            await SaveAsync();
        }

        public async Task DeletePositionAsync(Position position)
        {
            Delete(position);
            await SaveAsync();
        }
    }
}
