﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class InterviewerRepository : RepositoryBase<Interviewer>, IInterviewerRepository
    {
        public InterviewerRepository(interviewappdbContext repositoryContext) 
               : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Interviewer>> GetAllInterviewersAsync()
        {
            var interviewers = await FindAllAsync();
            return interviewers.OrderBy(x => x.FirstName);
        }

        public async Task<Interviewer> GetInterviewerByIdAsync(int interviewerId)
        {
            var interviewer = await FindByConditionAsync(o => o.InterviewerId.Equals(interviewerId));
            return interviewer.DefaultIfEmpty(new Interviewer()).FirstOrDefault();
        }

        public async Task CreateInterviewerAsync(Interviewer interviewer)
        {
            Create(interviewer);
            await SaveAsync();
        }

        public async Task UpdateInterviewerAsync(Interviewer dbInterviewer, Interviewer interviewer)
        {
            dbInterviewer.Map(interviewer);
            Update(dbInterviewer);
            await SaveAsync();
        }

        public async Task DeleteInterviewerAsync(Interviewer interviewer)
        {
            Delete(interviewer);
            await SaveAsync();
        }
    }
}
