﻿using Contracts;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private interviewappdbContext _repoContext;
        private IInterviewRepository _interview;
        private IInterviewerRepository _interviewer;
        private IInterviewRoundRepository _interviewRound;
        private IPositionRepository _position;
        private ICandidateRepository _candidate;
        private ICommentRepository _comment;

        public RepositoryWrapper(interviewappdbContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public IInterviewRepository Interview
        {
            get
            {
                if (_interview == null)
                    _interview = new InterviewRepository(_repoContext);
                return _interview;
            }
        }

        public IInterviewerRepository Interviewer
        {
            get
            {
                if (_interviewer == null)
                    _interviewer = new InterviewerRepository(_repoContext);
                return _interviewer;
            }
        }

        public IInterviewRoundRepository InterviewRound
        {
            get
            {
                if (_interviewRound == null)
                    _interviewRound = new InterviewRoundRepository(_repoContext);
                return _interviewRound;
            }
        }

        public IPositionRepository Position
        {
            get
            {
                if (_position == null)
                    _position = new PositionRepository(_repoContext);
                return _position;
            }
        }

        public ICandidateRepository Candidate
        {
            get
            {
                if (_candidate == null)
                    _candidate = new CandidateRepository(_repoContext);
                return _candidate;
            }
        }

        public ICommentRepository Comment
        {
            get
            {
                if (_comment == null)
                    _comment = new CommentRepository(_repoContext);
                return _comment;
            }
        }
    }
}
