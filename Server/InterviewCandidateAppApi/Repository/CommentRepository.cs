﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(interviewappdbContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Comment>> GetAllCommnetsAsync()
        {
            var comments = await FindAllAsync();
            return comments.OrderBy(x => x.Rate);
        }

        public async Task<Comment> GetCommentByIdAsync(int commentId)
        {
            var comment = await FindByConditionAsync(x => x.CommentId.Equals(commentId));
            return comment.DefaultIfEmpty(new Comment()).FirstOrDefault();
        }

        public async Task CreateCommentAsync(Comment comment)
        {
            Create(comment);
            await SaveAsync();
        }

        public async Task UpdateCommentAsync(Comment dbComment, Comment comment)
        {
            dbComment.Map(comment);
            Update(dbComment);
            await SaveAsync();
        }

        public async Task DeleteCommentAsync(Comment comment)
        {
            Delete(comment);
            await SaveAsync();
        }
    }
}
