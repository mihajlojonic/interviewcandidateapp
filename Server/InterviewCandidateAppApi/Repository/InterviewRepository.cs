﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class InterviewRepository : RepositoryBase<Interview>, IInterviewRepository
    {
        public InterviewRepository(interviewappdbContext repositoryContext) 
               : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Interview>> GetAllInterviewsAsync()
        {
            var interviews = await FindAllAsync();
            return interviews;
        }

        public async Task<Interview> GetInteviewByIdAsync(int interviewId)
        {
            var interview = await FindByConditionAsync(x => x.InterviewId.Equals(interviewId));
            return interview.DefaultIfEmpty(new Interview()).FirstOrDefault();
        }

        public async Task CreateInterviewAsync(Interview interview)
        {
            Create(interview);
            await SaveAsync();
        }

        public async Task UpdateInterviewAsync(Interview dbInterview, Interview interview)
        {
            dbInterview.Map(interview);
            Update(dbInterview);
            await SaveAsync();
        }

        public async Task DeleteInterviewAsync(Interview interview)
        {
            Delete(interview);
            await SaveAsync();
        }
    }
}
