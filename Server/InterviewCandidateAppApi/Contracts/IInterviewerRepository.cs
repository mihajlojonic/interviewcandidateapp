﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IInterviewerRepository : IRepositoryBase<Interviewer>
    {
        Task<IEnumerable<Interviewer>> GetAllInterviewersAsync();
        Task<Interviewer> GetInterviewerByIdAsync(int interviewerId);
        Task CreateInterviewerAsync(Interviewer interviewer);
        Task UpdateInterviewerAsync(Interviewer dbInterviewer, Interviewer interviewer);
        Task DeleteInterviewerAsync(Interviewer interviewer);
    }
}
