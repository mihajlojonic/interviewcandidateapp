﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IInterviewRoundRepository : IRepositoryBase<InterviewRound>
    {
        Task<IEnumerable<InterviewRound>> GetAllInterviewRoundsAsync();
        Task<InterviewRound> GetInterviewRoundByIdAsync(int interviewRoundId);
        Task CreateInterviewRoundAsync(InterviewRound interviewRound);
        Task UpdateInterviewRoundAsync(InterviewRound dbInterviewRound, InterviewRound interviewRound);
        Task DeleteInterviewRoundAsync(InterviewRound interviewRound);
    }
}
