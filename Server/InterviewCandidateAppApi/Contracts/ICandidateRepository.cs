﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface ICandidateRepository : IRepositoryBase<Candidate>
    {
        Task<IEnumerable<Candidate>> GetAllCandidates();
        Task<Candidate> GetCandidateByIdAsync(int candidateId);
        Task CreateCandidateAsync(Candidate candidate);
        Task UpdateCandidateAsync(Candidate dbCandidate, Candidate candidate);
        Task DeleteCandidateAsync(Candidate candidate);
    }
}
