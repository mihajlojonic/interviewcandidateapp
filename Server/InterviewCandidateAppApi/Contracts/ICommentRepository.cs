﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface ICommentRepository : IRepositoryBase<Comment>
    {
        Task<IEnumerable<Comment>> GetAllCommnetsAsync();
        Task<Comment> GetCommentByIdAsync(int commentId);
        Task CreateCommentAsync(Comment comment);
        Task UpdateCommentAsync(Comment dbComment, Comment comment);
        Task DeleteCommentAsync(Comment comment);
    }
}
