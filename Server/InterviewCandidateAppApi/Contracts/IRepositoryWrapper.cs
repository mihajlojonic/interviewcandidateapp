﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IInterviewRepository Interview { get; }
        IInterviewerRepository Interviewer { get; }
        IInterviewRoundRepository InterviewRound { get; }
        IPositionRepository Position { get; }
        ICandidateRepository Candidate { get; }
        ICommentRepository Comment { get; }
    }
}
