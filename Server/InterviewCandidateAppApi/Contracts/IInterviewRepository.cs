﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IInterviewRepository : IRepositoryBase<Interview>
    {
        Task<IEnumerable<Interview>> GetAllInterviewsAsync();
        Task<Interview> GetInteviewByIdAsync(int interviewId);
        Task CreateInterviewAsync(Interview interview);
        Task UpdateInterviewAsync(Interview dbInterview, Interview interview);
        Task DeleteInterviewAsync(Interview interview);
    }
}
