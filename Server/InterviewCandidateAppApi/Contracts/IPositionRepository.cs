﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IPositionRepository : IRepositoryBase<Position>
    {
        Task<IEnumerable<Position>> GetAllPosiotionsAsync();
        Task<Position> GetPositionByIdAsync(int positionId);
        Task CreatePositionAsync(Position position);
        Task UpdatePositionAsync(Position dbPosition, Position position);
        Task DeletePositionAsync(Position position);

    }
}
